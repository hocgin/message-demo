package in.hocg.bean;

import in.hocg.Receiver;

import java.util.Date;

public class UserNotify {

    private Notify notify;

    /**
     * 是否已读
     */
    private boolean isReady;

    /**
     * 接收者
     */
    private Receiver receiver;

    private Date sendAt;

    private Date readyAt;
}
