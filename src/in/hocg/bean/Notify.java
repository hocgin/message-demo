package in.hocg.bean;

import in.hocg.Sender;

import java.util.Date;

public class Notify {


    /**
     * 触发点
     * - 事件源
     * - 事件源类型
     * - 事件
     */
    private String eventSource; // 文章、店铺、用户
    private String eventSourceType;
    private String event; // 评论、点赞


    /**
     * 消息体
     */
    private String content;

    /**
     * 发送方：人
     */
    private Sender sender;

    /**
     * 消息类型
     * - 通知
     * - 私信
     */
    private int type;

    private Date createdAt;
}
