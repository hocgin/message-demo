package in.hocg.bean;

import in.hocg.Receiver;

import java.util.Date;

/**
 * 订阅相当于订阅了一组动作
 * 例如：订阅了一个店铺，就等于订阅了店铺的发布、上新等
 */
public class Subscription {

    /**
     * 触发点
     * - 事件源 ID
     * - 事件源类型
     * - 事件
     */
    private String eventSource; // 文章、店铺、用户
    private String eventSourceType;
    private String event; // 这边会对应一个组[]

    /**
     * 接收者
     */
    private Receiver receiver;

    private Date subscriptionAt;
}
