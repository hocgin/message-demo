package in.hocg.bean;

import in.hocg.User;

import java.util.HashMap;
import java.util.Map;

public class SubscriptionConfig {

    /**
     * 默认为 true
     *
     * article.comment = true
     * article.like = false
     */
    private Map<String, Boolean> settings = new HashMap<>();
    private User user;
}
