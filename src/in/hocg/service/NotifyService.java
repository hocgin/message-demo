package in.hocg.service;

import in.hocg.User;
import in.hocg.bean.Notify;
import in.hocg.bean.UserNotify;

import java.util.Date;
import java.util.List;

/**
 * type == 私信
 */
public interface NotifyService {

    void publish(String senderID, String receiverID, String content);

    void ready(String nID);

    List<UserNotify> getUnready(String receiverID);

    /**
     * []
     */
    List<UserNotify> getRange(Date startAt, Date endAt);
}
