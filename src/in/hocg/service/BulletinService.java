package in.hocg.service;

import in.hocg.bean.Subscription;
import in.hocg.bean.UserNotify;

import java.util.List;

/**
 * type == 公告
 */
public interface BulletinService {

    void publish(String senderID, String content);

    List<UserNotify> pull(String uID, String source, String type);
}
