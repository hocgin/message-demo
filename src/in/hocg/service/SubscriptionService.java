package in.hocg.service;

import in.hocg.bean.Subscription;
import in.hocg.bean.UserNotify;

import java.util.List;

/**
 * type == 通知
 */
public interface SubscriptionService {

    void publish(String senderID, String source, String type, String action);

    void subscription(String uID, String source, String type, String action);

    void unSubscription(String uID, String source, String type);

    List<UserNotify> pull(String uID, String source, String type);
}
